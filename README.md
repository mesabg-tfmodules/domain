# Domain Module

This module is capable to create a new Route53 DNS Zone with SSL certificates included.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - domain general name
- `domain` - domain name

Usage
-----

```hcl
module "domain" {
  source            = "git::https://gitlab.com/mesabg-tfmodules/domain.git?ref=v1.0.0"

  environment       = "environment"
  name              = "example"
  domain            = "example.com"
}
```

Outputs
=======

 - `route53_zone_arn` - Route 53 ARN


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
