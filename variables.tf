variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "Domain name"
}

variable "domain" {
  type        = string
  description = "Domain"
}
