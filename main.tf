resource "aws_route53_zone" "route53_zone" {
  name = var.domain

  tags = {
    Name        = var.domain
    Environment = var.environment
  }

  #lifecycle {
  #  prevent_destroy = true
  #}
}

resource "aws_acm_certificate" "acm_certificate" {
  domain_name               = var.domain
  subject_alternative_names = ["*.${var.domain}"]
  validation_method         = "DNS"

  tags = {
    Name        = var.name
    Environment = var.environment
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "route53_acm_certificate" {
  for_each = {
    for dvo in aws_acm_certificate.acm_certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.route53_zone.zone_id

  #lifecycle {
  #  prevent_destroy = true
  #}
}
