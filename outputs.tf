output "zone_arn" {
  value       = aws_route53_zone.route53_zone.arn
  description = "Route 53 Zone ARN"
}

output "zone_id" {
  value       = aws_route53_zone.route53_zone.id
  description = "Route 53 Zone ID"
}

output "certificate_arn" {
  value       = aws_acm_certificate.acm_certificate.arn
  description = "ACM Certificate ARN"
}
